import torch 
time_step = 1000
batch_size = 512
num_channel = 15
lr = 0.0001
num_class = 2
thr_out = 5.0
num_epochs = 200
device = torch.device('cuda:3' if torch.cuda.is_available() else 'cpu')