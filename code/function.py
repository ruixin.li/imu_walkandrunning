import numpy as np
import tqdm
from torch.utils.data import Dataset
from parameter import *


def split_array(array, chunk_size):
    num_chunks = array.shape[0] // chunk_size
    split_arrays = np.array_split(array[:num_chunks * chunk_size], num_chunks)
    return split_arrays

def detect_spike_analog(rec,thr,no_spike_num):
    first_surpass_thr = []
    data = rec['4_LIFExodus']['vmem']
    for i in range(data.shape[0]):
        if torch.sum((data[i, :, :].squeeze())>thr).item() == 0:
            first_surpass_thr.append(data[i,time_step-1,:].cpu().detach().numpy())
            no_spike_num+=1
        else:
            one_batch_data = ((data[i, :, :].squeeze())>thr).nonzero()
            first_surpass_thr.append(data[i,one_batch_data[0][0].item(),:].cpu().detach().numpy())
    return np.asarray(first_surpass_thr) ,no_spike_num

def detect_spike_Xylo(out,rec):
    if out.sum() != 0:
        first_nonzero_rows = out[np.nonzero(out)[0][0]]
        return first_nonzero_rows
    else:
        last_vmem = rec['Vmem_out'][-1]
        return last_vmem
    
def print_colorful_text(text, color):
    colors = {
        'red': '\033[91m',
        'green': '\033[92m',
        'yellow': '\033[93m',
        'blue': '\033[94m',
        'magenta': '\033[95m',
        'cyan': '\033[96m',
    }
    end_color = '\033[0m'
    
    if color in colors:
        print(f"{colors[color]}{text}{end_color}")
    else:
        print(text)
        
        
def generate_pre_by_spike(rec,thr_run,thr_walk):
    data = rec['Vmem_out'].T[:,400:]
    label_r = data[0]>thr_run
    label_w = data[1]>thr_walk
    label = label_r | label_w
    try:
        first_spike_locate = np.where(label==1)[0][0]
        if (label_r[first_spike_locate]) :
            result = 0
        else: result = 1
    except:
        print('awdawd')
    return result
