import torch.nn as nn
from parameter import *
from rockpool.nn.modules import LIFExodus, LIFTorch
from rockpool.nn.networks import SynNet

if str(device) == 'cpu':
    neuron_model = LIFTorch
else:
    neuron_model = LIFExodus
print(device)
print(neuron_model)


# synnet_spike = SynNet(
#     n_channels=num_channel,
#     n_classes=num_class,
#     size_hidden_layers = [60],
#     output='spikes',
#     neuron_model=neuron_model,
#     tau_syn_base = 0.002,
#     tau_mem = 0.002,
#     tau_syn_out = 0.002,
#     threshold_out=thr_out
# )
synnet_vmem = SynNet(
    n_channels=num_channel,
    n_classes=num_class,
    size_hidden_layers = [60],
    output='vmem',
    neuron_model=LIFExodus,
    tau_syn_base = 0.002,
    tau_mem = 0.002,
    tau_syn_out = 0.002,
)
synnet_fspike = SynNet(
    n_channels=num_channel,
    n_classes=num_class,
    size_hidden_layers = [60],
    output='vmem',
    neuron_model=neuron_model,
    tau_syn_base = 0.002,
    tau_mem = 0.02,
    tau_syn_out = 0.002,
)


class Myann(nn.Module):
    def __init__(self):
        super(Myann, self).__init__()
        self.conv1 = nn.Conv2d(1, 32, kernel_size=3, stride=1)
        self.conv2 = nn.Conv2d(32, 64, kernel_size=(4, 1), stride=1)
        self.pool = nn.MaxPool2d(kernel_size=2, stride=2)
        self.fc1 = nn.Linear(75264, 64)
        self.fc2 = nn.Linear(64, num_class)
        self.relu = nn.ReLU()
        self.softmax = nn.Softmax(dim=1)

    def forward(self, x):
        x = self.relu(self.conv1(x.float()))
        x = self.pool(x)
        x = self.relu(self.conv2(x))
        # x = self.pool(x)
        x = x.view(x.size(0), -1)
        x = self.relu(self.fc1(x))
        output = self.fc2(x)
        # output = self.softmax(x)
        return output
    
ann = Myann()