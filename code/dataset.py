from torch.utils.data import Dataset
from pathlib import Path
from function import *
from encoder import *
from tqdm.auto import tqdm

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import torch
import random
class IMU_dataset(Dataset):
    def __init__(self, data, targets):
        self.data = []
        self.targets = []
        imu = create_imu_encoder()
        print('encoding...')
        for i,j in tqdm(zip(data, targets)):
            spike = generate_spikes(i.T,imu)
            self.data.append(torch.tensor(spike.T))
            self.targets.append(j)        
        pass
    
    def __getitem__(self, index):
        x = self.data[index]
        y = self.targets[index]
        return x, y

    def __len__(self):
        return len(self.data)
    


def generate_data(path):
    file = Path(path)
    file_list = list(file.rglob('*.csv'))
    # random.shuffle(file_list)
    data_list = []
    label_list = []
    for file in tqdm(file_list):
        raw = pd.read_csv(str(file))
        data = (raw.iloc[:,1:4]).values
        split_arrays = split_array(data, chunk_size=time_step)
        # 0:jog, 1:walk
        label = 0 if file.parts[-4][0] == 'j' else 1
        data_list.extend(split_arrays)
        label_list = label_list + [label]*len(split_arrays)
    return data_list, label_list 


