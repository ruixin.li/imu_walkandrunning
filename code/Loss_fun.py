from parameter import *

def CACULATE_LOSS(thr: float, 
                  data: torch.Tensor,
                  label: torch.Tensor,
                  criterion=(torch.nn.MSELoss()).to(device)):
    
    """
    Calculate the loss based on the given thresholds, data, and labels.
    "This function is designed to enhance the modification of the last layer neural membrane potential (vmem) corresponding to the correct class while mitigating changes to the vmem of neurons associated with incorrect classes."
    Parameters:
        thr (float): The threshold value.
        data (torch.Tensor): Input data tensor of shape (batch_size, time_step, num_class).
        label (torch.Tensor): Label tensor of shape (batch_size).
        num_class (int): The number of classes.
        time_step (int): The number of time steps.
        criterion (torch.nn.Module): The loss criterion, e.g., torch.nn.MSELoss().

    Returns:
        torch.Tensor: The calculated loss.
    """
    label = label.to(device)
    data = data.to(device)
    result_rows = []
    # Iterate over each label value
    for i in range(label.size(0)):
        # Get the current label value
        current_label = label[i].item()
        # Generate an index list, excluding the row corresponding to the current label value
        indices_to_select = [j for j in range(num_class) if j != current_label]
        # Use the index list to select the corresponding rows and add them to the result list
        selected_rows = data[i, indices_to_select, :]
        result_rows.append(selected_rows)
    data_need_restrain = torch.stack(result_rows)
    loss_A = torch.relu(data_need_restrain-(thr)).sum()
    
    data_need_simulated = data[torch.arange(len(label)), label, :].unsqueeze(1)
    loss_B_sum =0
    for channel in data_need_simulated:
        if  (channel < thr_out+1.0).all(dim = 1):
            loss_B = criterion(channel.mean(),torch.tensor(thr_out+1.0).to(device))
            loss_B_sum += loss_B 
        else : pass
        
    return loss_A + loss_B_sum